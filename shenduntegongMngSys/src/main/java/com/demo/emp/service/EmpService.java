package com.demo.emp.service;

import com.demo.emp.entity.Dept;
import com.demo.emp.entity.Emp;
import com.demo.emp.vo.EmpQuery;

import java.util.List;

public interface EmpService {
    List<Emp> getEmpList(EmpQuery empQuery);
    Long getEmpCount(EmpQuery empQuery);
    void addEmp(Emp emp);
    List<Dept> getAllDept();
    void deleteEmpByIds(String ids);
    Emp getEmpById(Integer id);
    void updateEmp(Emp emp);
}
