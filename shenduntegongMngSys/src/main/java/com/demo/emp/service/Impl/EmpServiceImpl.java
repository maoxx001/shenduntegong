package com.demo.emp.service.Impl;

import com.demo.emp.entity.Dept;
import com.demo.emp.entity.Emp;
import com.demo.emp.mapper.DeptMapper;
import com.demo.emp.mapper.EmpMapper;
import com.demo.emp.service.EmpService;
import com.demo.emp.vo.EmpQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmpServiceImpl implements EmpService {

    @Autowired
    private EmpMapper empMapper;

    @Autowired
    private DeptMapper deptMapper;

    @Override
    public List<Emp> getEmpList(EmpQuery empQuery) {
        return empMapper.getEmpList(empQuery);
    }

    @Override
    public Long getEmpCount(EmpQuery empQuery) {
        return empMapper.getEmpCount(empQuery);
    }

    @Override
    public void addEmp(Emp emp) {
        empMapper.addEmp(emp);
    }

    @Override
    public List<Dept> getAllDept() {
        return deptMapper.getAllDept();
    }

    @Override
    public void deleteEmpByIds(String ids) {
        empMapper.deleteEmpByIds(ids);
    }

    @Override
    public void updateEmp(Emp emp) {
        empMapper.updateEmp(emp);
    }

    @Override
    public Emp getEmpById(Integer id) {
        return empMapper.getEmpById(id);
    }
}
