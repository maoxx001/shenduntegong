package com.demo.emp.vo;


import com.demo.common.vo.Page;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class EmpQuery extends Page {
    private String name;

    @DateTimeFormat(pattern = "yyyy-MM-dd") // 日期格式化,否则会报错
    private Date startDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;
}
