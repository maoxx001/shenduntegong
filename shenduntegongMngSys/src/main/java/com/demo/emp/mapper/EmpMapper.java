package com.demo.emp.mapper;

import com.demo.emp.entity.Emp;
import com.demo.emp.vo.EmpQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface EmpMapper {
    List<Emp> getEmpList(EmpQuery empQuery);
    Long getEmpCount(EmpQuery empQuery);
    void addEmp(Emp emp);
    void deleteEmpByIds(String ids);
    Emp getEmpById(Integer id);
    void updateEmp(Emp emp);
}
