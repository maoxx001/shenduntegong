package com.demo.emp.mapper;

import com.demo.emp.entity.Dept;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DeptMapper {

    List<Dept> getAllDept();
}
