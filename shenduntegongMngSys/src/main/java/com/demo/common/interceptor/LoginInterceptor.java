package com.demo.common.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Object user = request.getSession().getAttribute("userInfo"); // 获取session中的用户信息
        if (user == null) {
            response.sendRedirect(request.getContextPath() + "/login"); // 未登录，跳转到登录页面
            return false;
        }
        return true; // 放行
    }
}
