package com.demo.common.exception;


import com.demo.common.vo.Result;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice // 全局异常处理
public class MyExceptionHandler   {

    @ExceptionHandler(Exception.class) // 处理所有异常
    @ResponseBody
    public Result<Object> handlerException(Exception e){
        e.printStackTrace();
        return Result.fail("系统出现错误：" + e.getMessage());
    }

}
