package com.demo.common.vo;


import lombok.Data;

/**
 * 创建一个统一的结果类
 * 主要是
 */

@Data
public class Result<T> {
    private Integer code; // 返回码，0表示成功，1表示失败
    private String msg; // 返回信息
    private T data; // 返回数据,，因为不知道返回什么类型数据，所以用泛型
    private Long count; // 分页查询的总共记录数

    private Result() {}

    private Result(Integer code, String msg, T data, Long count) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.count = count;
    }

    public static  Result<Object> success() { // 成功
        return new Result<Object>(0, "success", null, null);
    }

    public static  Result<Object> success(String msg) { // 返回添加成功信息
        return new Result<Object>(0, msg, null, null);
    }

    public static  Result<Object> success(Object data, Long count) { // 成功
        return new Result<Object>(0, "success", data, count);
    }


    public static  Result<Object> fail() { // 失败
        return new Result<Object>(-1, "fail", null, null);
    }

    public static  Result<Object> fail(String msg) { // 失败
        return new Result<Object>(-1, msg, null, null);
    }
}
