package com.demo.common.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class Page implements Serializable {
    private Integer page;
    private Integer limit;

    Long getStart() {
        return (this.page - 1L) * this.limit;
    }
}
