package com.demo.user.service;

import com.demo.user.pojo.User;

public interface UserService {
    User login(User user);
}
