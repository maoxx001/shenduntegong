package com.demo.user.service.Impl;

import com.demo.user.mapper.UserMapper;
import com.demo.user.pojo.User;
import com.demo.user.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public User login(User user) {
        return userMapper.login(user);
    }
}
